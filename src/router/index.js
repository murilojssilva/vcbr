import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Apartamentos from '../views/propriedades/Apartamentos'
import Casas from '../views/propriedades/Casas'
import Edificios from '../views/propriedades/Edificios'
import Terreno from '../views/propriedades/Terreno'
import Oficinas from '../views/propriedades/Oficinas'
import LocalComercial from '../views/propriedades/Apartamentos'
import PropriedadesDePlaya from '../views/propriedades/PropriedadesDePlaya'
import Nosotros from '../views/nosotros/Nosotros'
import Contactos from '../views/nosotros/Contactos'
import Servicios from '../views/nosotros/Servicios'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: Home
  },
  {
    path: '/apartamentos',
    component: Apartamentos
  },
  {
    path: '/casas',
    component: Casas
  },
  {
    path: '/edificios',
    component: Edificios
  },
  {
    path: '/oficinas',
    component: Oficinas
  },
  {
    path: '/local-comercial',
    component: LocalComercial
  },
  {
    path: '/terreno',
    component: Terreno
  },
  {
    path: '/propriedades-de-playa',
    component: PropriedadesDePlaya
  },
  {
    path: '/nosotros',
    component: Nosotros
  },
  {
    path: '/servicios',
    component: Servicios
  },
  {
    path: '/contactos',
    component: Contactos
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
